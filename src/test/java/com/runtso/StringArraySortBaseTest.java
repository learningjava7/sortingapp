package com.runtso;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Unit test for StringArraySort - base cases
 */
@RunWith(Parameterized.class)
public class StringArraySortBaseTest extends TestCase {
    String[] args;
    String expected;


    public StringArraySortBaseTest(String[] args, String expected) {
        this.args = args;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> sourceArgs() {
        return Arrays.asList(new Object[][] {
                {new String[]{"20"}, "20"},                                 //normal case one element
                //normal case maximum elements
                {new String[]{"10", "1", "3", "2", "8", "9", "7", "4", "6", "5"}, "1 2 3 4 5 6 7 8 9 10"},
                //normal case maximum elements - 1
                {new String[]{"1", "3", "2", "8", "9", "7", "4", "6", "5"}, "1 2 3 4 5 6 7 8 9"},
                {new String[]{"1", "15", "23"}, "1 15 23"},                 //normal case with already sorted args
                {new String[]{"-30", "-20", "-100"}, "-100 -30 -20"}        //normal case with negative args
        });
    }

    @Test
    public void testBaseCases() {
        StringArraySort stringArraySort = new StringArraySort();
        assertEquals(expected, stringArraySort.sort(args));
    }
}
