package com.runtso;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Unit test for StringArraySort - IllegalArgumentException cases
 */
@RunWith(Parameterized.class)
public class StringArraySortExceptionTest extends TestCase {
    String[] args;

    public StringArraySortExceptionTest(String[] args) {
        this.args = args;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> sourceArgs() {
        return Arrays.asList(new Object[][] {
                {null},             //null case
                {new String[]{}},   //zero case
                {new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"}},  //more than 10
                {new String[]{"1f", "a", "b"}},  //non integer values
                {new String[]{"1.0"}},  //non integer values
                {new String[]{"1,0"}},  //non integer values
                {new String[]{"0xA"}}   //non integer values
        });
    }

    @Test (expected = IllegalArgumentException.class)
    public void testExceptionCases() {
        StringArraySort stringArraySort = new StringArraySort();
        stringArraySort.sort(args);
    }
}
