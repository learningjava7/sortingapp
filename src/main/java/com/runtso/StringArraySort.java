package com.runtso;

import java.util.Arrays;

/**
 * Class implements validation and converting string array to integer array, sort it and return as string.
 * <p>Have default restriction of maximum array length MAX_LENGTH = 10</p>
*/
public class StringArraySort {
    /**
     * Maximum array length
     */
    private static final int MAX_LENGTH = 10;

    /**
     * Validate string array (by private method validate) convert string array to integer array,
     * sort it and return as string.
     * @param strArr    String array with integer values (other values will throw IllegalArgumentException) and
     *                  maximum length of array is MAX_LENGTH
     * @return  String contains up to MAX_LENGTH sorted values separated by spaces
     */
    public String sort(String[] strArr) {
        validate(strArr);                   //throw IllegalArgumentException if strArr includes not valid values
        int[] intArr = toIntArr(strArr);

        Arrays.sort(intArr);   //use standard lib

        StringBuilder resultStr = new StringBuilder();

        for (int i : intArr) {
            resultStr.append(i).append(" ");
        }

        return resultStr.toString().trim();
    }

    /**
     * Validate string array. It must include only integer values
     * @param strArr String array with integer values (other values will throw IllegalArgumentException)
     */
    private void validate(String[] strArr) {
        if (strArr == null) {
            throw new IllegalArgumentException("Input array is null");
        }
        if (strArr.length == 0) {
            throw new IllegalArgumentException("Input array is empty");
        }
        if (strArr.length > MAX_LENGTH) {
            throw new IllegalArgumentException(String.format("Input array has illegal length: %d", strArr.length));
        }
        for (String s : strArr) {
            try {
                Integer.parseInt(s);
            }
            catch (Exception e) {
                throw new IllegalArgumentException("Input array has illegal symbol: " + s);
            }
        }
    }

    /**
     * Converting string array to integer array
     * @param strArr String array with integer values only
     * @return Integer array
     */
    private int[] toIntArr(String[] strArr) {
        int[] intArr = new int[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            intArr[i] = Integer.parseInt(strArr[i]);
        }
        return intArr;
    }
}
