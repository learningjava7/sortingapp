package com.runtso;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * SortingApp - get from command line up to ten integer number, sorts them and output to standard output.
 * <p>Non integer arguments will throw IllegalArgumentException.</p>
 *
 * @author Runtso Andrei
 * @version 1.2
 * @since 2022-12-22
 */
public class SortingApp {
//    private static final ch.qos.logback.classic.Logger logger =
//            (ch.qos.logback.classic.Logger)LoggerFactory.getLogger(SortingApp.class);
    private static final Logger logger =
            LoggerFactory.getLogger(SortingApp.class);
    /**
     * Receives args from command line converts it to integer array, sorts and prints to System.out
     *
     * @param args - String array from command line
     */
    public static void main(String[] args) {
        logger.debug("Input string array: {}", Arrays.toString(args));

        StringArraySort stringArraySort = new StringArraySort();
        try {
            System.out.println(stringArraySort.sort(args));
        }
        catch (IllegalArgumentException e) {
            logger.error(e.getMessage());
        }
    }
}
